# GlideinWMS Frontend Operations Manual 

GlideinWMS is a pilot-based workload management system that acts as an abstraction layer for grid sites. When a work flow submits jobs that ask to be run at, for example, "T1_US_FNAL", the glideinWMS system creates condor jobs ("glideins") properly configured for T1 FNAL site, and distributes them evenly across the FNAL computing elements. The glidein system works on a system of pressure - the more pending jobs, the more glideins created - but scaled back in order to ensure that the site doesn't get overwhelmed with requests. There are four primary machine types in a glideinWMS pool:

 * Central Manager: This node runs the collector daemon (responsible for collecting information from all other daemons) and the negotiator daemon (responsible for matching jobs to glideins).
 * Schedd: Jobs are queued in these machines by WMAgent, CRAB, CMS Connect, and outside institute schedd's.
 * GlideinWMS Frontend: Matches the jobs to grid sites based on job requirements and site specifications, and requests pilots from the glideinWMS factory accordingly.
 * GlideinWMS Factory: GlideinWMS factory contains all the site entries. It submits glideins to the site gatekeepers depending upon the requests by the glideinWMS frontend


