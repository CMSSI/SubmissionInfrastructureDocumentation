# Tokens Issued

| Key                | Subject                           | Grants                                               | UUID                             | Issued     | Expiry     | Contact Name    | Contact Email / tbag host              |
| ------------------ | --------------------------------- | ---------------------------------------------------- | -------------------------------- | ---------- | ---------- | --------------- | -------------------------------------- |
| itb_pool_key       | thmadlen                          | ADVERTISE_MASTER, ADVERTISE_STARTD, ADVERTISE_SCHEDD | 8ae4d25e1610bc39714e66bdde018cd4 | 1730889999 | 1762425999 | Thomas Madlener | thomas.madlener@cern.ch                |
| itb_pool_key       | PIC-BSC@cms                       | ADVERTISE_MASTER, ADVERTISE_STARTD, READ             | 4995c257416ff5cf79fc2e9ce511c57d | 1728310939 | 1759846939 | Antonio         | antonio.perez.calero.yzquierdo@cern.ch |
| volunteer_pool_key | CNAF_VEGA_gpu                     | ADVERTISE_MASTER, ADVERTISE_STARTD, ADVERTISE_SCHEDD | dcd95c6e0d2260a612ffa1c5aad966d9 | 1706025336 | 1737561336 | Daniele Spiga   | daniele.spiga@cern.ch                  |
| global_pool_key    | CNAF_VEGA_gpu@cms                 | ADVERTISE_MASTER, ADVERTISE_STARTD, ADVERTISE_SCHEDD | 96bb8bb58076699391aac81d9b0a47ac | Feb04th24  | Feb04th25  | Daniele Spiga   | daniele.spiga@cern.ch                  |
| itb_pool_key       | CNAF_VEGA_gpu@cms                 | ADVERTISE_MASTER, ADVERTISE_STARTD, ADVERTISE_SCHEDD | af9fb917bf731170c67e643f63b5ded2 | Feb04th24  | Feb04th25  | Daniele Spiga   | daniele.spiga@cern.ch                  |
| itb_pool_key       | UCSD_PRP_gpu                      | ADVERTISE_MASTER, ADVERTISE_STARTD, ADVERTISE_SCHEDD |                                  | Feb24th24  | Feb24th25  | Igor Sfiligoi   | isfiligoi@sdsc.edu                     |
| itb_pool_key       | crabschedd_crab-sched-901@cern.ch | ADVERTISE_MASTER, ADVERTISE_SCHEDD, ADVERTISE_SCHEDD | cac0cdfc061bcf57113d9e931ff2355b | 1709288729 | 1740824729 | Stefano         | `crab-sched-901`                       |
| itb_pool_key       | crabschedd_crab-sched-903@cern.ch | ADVERTISE_MASTER, ADVERTISE_SCHEDD, ADVERTISE_SCHEDD | 402c9547e73a67c5fcbba9dea806792f | 1709288740 | 1740824740 | Stefano         | `crab-sched-903`                       |
| global_pool_key    | crabschedd_crab-sched-901@cern.ch | ADVERTISE_MASTER, ADVERTISE_SCHEDD, ADVERTISE_SCHEDD | fac368da6c919022b310d378b1bc6a7a | 1710408013 | 1741944013 | Stefano         | `crab-sched-901`                       |
| global_pool_key    | crabschedd_crab-sched-903@cern.ch | ADVERTISE_MASTER, ADVERTISE_SCHEDD, ADVERTISE_SCHEDD | de4d89b152753035998b0df2ad19be54 | 1710408028 | 1741944028 | Stefano         | `crab-sched-903`                       |
| tier0_pool_key     | crabschedd_crab-sched-901@cern.ch | ADVERTISE_MASTER, ADVERTISE_SCHEDD, ADVERTISE_SCHEDD | 54bac1853f97dae22e83e8dccbf5ac71 | 1710413688 | 1741949688 | Stefano         | `crab-sched-901`                       |
| tier0_pool_key     | crabschedd_crab-sched-903@cern.ch | ADVERTISE_MASTER, ADVERTISE_SCHEDD, ADVERTISE_SCHEDD | fb28f0bd67beaaae0e885e785a7af96c | 1710413700 | 1741949700 | Stefano         | `crab-sched-903`                       |


!!! example "Token creation example"

    `[root@vocms0807 ~]# condor_token_create -authz ADVERTISE_MASTER -authz ADVERTISE_STARTD -authz READ -identity "CINECA_Marconi100@cms" -key itb_pool_key -lifetime 31622400`
