# Submission Infrastructure Overview

This is the internal documentation of the Submission Infrastructure group.

In the Submission Infrastructure team our duty is to manage and operate the
computing resources that have been provided to CMS and
serve the requests for computing resources of CMS teams and users.
This is achieved using the [HTCondor Software
Suite](https://htcondor.org/index.html). The suit includes software to collect
jobs (schedulers), to collect the available resources/computing power
(collectors) and to perform the matchmaking between the jobs and resources
(negotiator).

In addition, [GlideinWMS](https://glideinwms.fnal.gov/doc.prd/index.html) is
used in order to dynamically scale the resource requests to ensure that periods
of high demand are handled, while also avoiding having a lot of idle resources
in periods of low demand.

For the operation of the infrastructure please refer to the documentation of the
relevant piece of software.

* [HTCondor documentation](https://htcondor.readthedocs.io/en/latest/)

* [GlideinWMS documentation](https://glideinwms.fnal.gov/doc.prd/documentation.html)
