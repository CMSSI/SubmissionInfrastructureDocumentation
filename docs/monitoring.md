# Monitoring (WIP)

For now this is a random collection of stuff related to our monitoring.

* "Antonio's monitoring" on [GitHub](https://github.com/aperezca/CMS-global-pool-monitor/tree/master)
* SI monitoring on [GitLab](https://gitlab.cern.ch/CMSSI/SubmissionInfrastructureMonitoring/)
    * Data is aggregated on host `vocms0852` as user `monitor`. Check `crontab -l`.
    * Data is sent to elasticsearch with [amq](https://monit-docs.web.cern.ch/metrics/amq/)
        * We have two [ActiveMQ](https://mig-user.docs.cern.ch/glossary.html#activemq) topics: `/topic/cms.si.condor` and `/topic/cms.si.condor.lt`
        * The former is used for our current monitoring, the latter is not yet used, but foreseen for long term storage

# ElasticSearch Indexes and ClassAd Propagation

This section describes the relevant ElasticSearch indexes and the process of propagating machine attributes to jobs in the Condor monitoring system.

## ElasticSearch Indexes for Jobs and Machines

There are different ElasticSearch indexes that are typically used to track machines and jobs in the monitoring system:

1. **`monit_prod_cms_raw_si_condor_startd*`**  
   This index contains information about machines (and the jobs running on them), essentially the result of running `condor_status` on the central manager.

2. **`monit_prod_condor_raw_metric*`**  
   This index holds jobs' classads, essentially the result of running `condor_q` on the schedulers.

## Propagation of Attributes

### 1. **Machine Index (`monit_prod_cms_raw_si_condor_*`)**

To propagate machine attributes, we need to update the field in the repository [here](https://gitlab.cern.ch/CMSSI/SubmissionInfrastructureMonitoring/-/blob/master/monit/classAds/startd?ref_type=heads).  
Note: Not all attributes from `condor_status` are propagated to ElasticSearch by default.

### 2. **Job Index (`monit_prod_condor_raw_metric*`)**

Propagating machine attributes to jobs is more complex since the attributes are not part of the classads for jobs by default. To address this, we propagate the relevant attributes from machines to jobs by using the `SYSTEM_JOB_MACHINE_ATTRS` variable. 

Once this attribute is available on the schedulers, the CMS monitoring team will likely need to add it to a list of attributes on their side as well.

### Example Configuration for `SYSTEM_JOB_MACHINE_ATTRS`

Here is an example from `vocms0155` showing the configuration for propagating machine attributes to jobs:

```bash
[mmascher@vocms0155 ~]$ grep SYSTEM_JOB_MACHINE_ATTRS /etc/condor/config.d/80_cms_schedd_generic.config
SYSTEM_JOB_MACHINE_ATTRS = $(SYSTEM_JOB_MACHINE_ATTRS) CMSSubSiteName CUDACapability CUDAClockMhz CUDAComputeUnits CUDACoresPerCU CUDADeviceName CUDADriverVersion CUDAECCEnabled CUDAGlobalMemoryMb CMSProcessingSiteName CpuModelName CpuModelNumber Arch Microarch GLIDEIN_OVERLOAD_ENABLED

