<p><img src="../img/SI.jpg" alt="Submission Infrastructure logo" width="200" height="200"></p>

# SUBMISSION INFRASTRUCTURE PERFORMANCE GOALS AND MILESTONES FOR LATE 2018 & EARLY 2019
 * Immediate: Open a call for a new CAT-A:[job description](https://twiki.cern.ch/twiki/bin/view/CMS/MnOAJobs)

## Transitional Period (up to March 1, 2019):
  
 * GlideinWMS: Periodic Validation roll-out
 * Documentation: to be reviewed and updated (~1w)
 * Complete CentOS7 migration of central services PRIORITY
    * CCB: Careful with HLT
    * Frontends: Need to test glideinWMS 3.4.2 with shared port in the ITB, then migrate to CentOS7. Do scale test (below) if there is time. 
 * Tutorial projects
    * HTCondor: Current version 8.7.8, move to 8.8.0.
    * Move Tier-0 central infrastructure (after Global Pool) to CentOS7. 

## After March 1, 2019:

* Scale Testing in the ITB:
    * New multi-threaded Negotiator
    * Running without schedd claim leftovers 
* MonIT Integration: IN PROGRESS
    * Documentation : in Diego's slides 
* GlideinWMS Improvements:
    * Multi-node glideins
    * Integrate meta-sites solution
    * Decoupling pilot proxy lifetime and glidein lifetime
    * Eliminate unprojected frontend queries 
* Global Pool: Debugging connection problems of CCB's with some WN's at DESY, KIT, IN2P3.
* Tier-0 pool: clean up frontend configuration 

## Later in 2019:

* Ticketing: Move to a ticketing system. JIRA? NEW
* HPC/Cloud/Opportunistic Evolution:
    * Validation
    * Sending pilots or payloads
    * Local matching conditions 
* Security:
    * Integrate SI into security incident procedures
    * User banning at a site NEW
    * Blocking queries NEW 
* CRIC Integration (e.g. multi-core ready sites file)
* IPv6 critical path: HA infrastructure at FNAL is not dual-stack (ETA?)

#SUBMISSION INFRASTRUCTURE PERFORMANCE GOALS AND MILESTONES FOR 2018

A [talk](https://indico.cern.ch/event/625239/contributions/2784821/attachments/1557760/2450638/20171114_SI_status_milestones.pdf) on 2018 milestones was presented in November at the Offline and Computing Week at CERN.

## Performance goals
Can we develop some scheduling efficiency and up-time performance goals for 2018? 

## Milestones

Project milestones with desired dates for completion:

* Analysis/Production Fair Share:
    * Fix dynamic vs. static quotas - Goal to Complete: January 31, 2018. - DONE Feb 5th 
* Migrate the Tier-0 to HTCondor at CERN, unifying with T2_CH_CERN. See the Planning Document - Goal to complete: March 1, 2018. Apart from waiting for an upstream patch from the HTCondor developers to prevent normal jobs from matching the Ioslots and Repackslots, this project was completed in mid-March.
* Migrate the CAF functionality to T2_CH_CERN. end of year?
* Scale Testing in the ITB to study:
    * Multiple Negotiators/Multiple federated pools - DONE
    * New multi-threaded Negotiator - NOT YET DONE DEC 2018
    * Running without schedd claim leftovers - NOT YET DONE DEC 2018
    * Pushing dynamic slot limitations to higher scales 
* Concurrency Limits: (low priority) - ABANDONED EARLY 2018
    * Scheduling I/O:
        * in a glidein - exists, just not fully integrated - needed?
        * at a site (so as not to overwhelm the storage) (N.B. Tier-0 will simply use the dedicated high-I/O slot) - low priority now, as Unified can control these levels itself
        * over WAN paths - 2019? 
* GlideinWMS Improvements:
    * Evaluate next-generation solutions for more tightly coupling glidein supply to instantaneous resource demand - need to upgrade to glideinWMS 3.2.21 April 2018
    * Integrate meta-sites solution
    * Decoupling pilot proxy lifetime and glidein lifetime
    * Eliminate unprojected frontend queries - NEW 
* HTCondor Improvements:
    * Queue schedd queries? (Slow schedd's still affecting the negotiation cycle length adversely.) - NEW
    * HA-Schedd (Loriano, on his schedule)
    * Mechanism to suspend temporarily the matching of production jobs to a site on the fly. Status: coded. Goal to integrate: March 1, 2018 
* Monitoring:
    * Dashboard integration for MIT schedd - however, CMS may be moving away from Dashboard in 2018. MonIT integration is enough?
    * MonIT integration - depends on how CMS evolves the monitoring forum to a L2 area? 
* Cloud
    * Integrate HEPCloud resources into the Global Pool - Goal to complete: December 31, 2018
        * Major project (if agreed upon): Could SI participate in integrating the Decision Engine as a glideinWMS frontend replacement? Goal to complete: December 31, 2018 
    * Integrate DODAS resources into the Global Pool - Goal to complete: March 1, 2018
        * Needs method to restrict DN to site name, one-off (possible?) or generalized list O(10) sites. 
* IPv6 and SL7:
    * Dual-stack glideinWMS factory
    * Complete SL7 migration of HTCondor and glideinWMS components 
* Revisit security procedures, lines of communication. Security incident drill. - NOT DONE
* Interactive access to GPU resources: Goal is to allow users use GPU resources with the proper containers needed to use machine learning tools. Some pre-requirement details: Requires a recent change in the HTCondor version used by the pilot, not available in the dev branch yet, but should be soon. - DONE?
* ClassAd rounding for RequestMemory & RequestDisk was removed from the WMAgent schedd’s earlier this year, but SI have observed a marked increased in the number of auto-clusters - to be investigated further, rolled back? - DONE
* Investigate replacing RSS by PSS in Memory usage calculation in HTCondor. - DONE? 

Proposed milestones:

* Upgrade the FNAL central manager infrastructure hardware. 
