# Secrets

There are 2 categories of secrets that are being managed. a) passwords (aka signing keys) used by Condor and GlideinWMS to generate and verify tokens and b) the tokens that are used by the condor daemons to authenticate themselves.

Secrets are stored in the Teigi secret storage service. Refer to the [configdocs](https://configdocs.web.cern.ch/secrets/index.html) for details. Secrets are stored and access-controlled on a hostgroup level. The users allowed to access them is the same set of users that have write access to the hostgroup (hint: use `ai-pwn show <hg>` to find out).
To place them in the correct place the teigi puppet module is used.

The naming scheme for the secret is as follows:

[passsword|token]\_[pool]\_[secret_name]

e.g.:

* password_global_frontend, the signing key for the global pool
* token_itb_factory0205: The token for the itb frontend to authenticate to factory0205.

You can list the available secrets with the following command:
```
$ tbag showkeys --hg vocms/glideinwms/gwmsfrontend
```
