# Overview

The configuration management tool used at CERN is puppet. There is comprehensive
guide provided by CERN IT explaining various aspects of puppet deployment and
its utilization at CERN. Please use this 
[link](http://configdocs.web.cern.ch/configdocs/overview/system.html) to read
this documentation.

When a puppetized machine is created it is assigned with a profile that is 
composed of a general category, called "hostgroup", and one or more 
subcategories or subhostgroups, within a hierarchical order. This profile
will determine the set of configurations to be applied within the machine. 
An agent in the machine will run periodically and make sure to keep the node 
updated with its profile.

In order to understand how the puppet setup works in our Infrastructure, there
are few concepts you need to ne familiar with: hostgroup, subhostgroup, module
and hiera variables.

## Hostgroup

It is a general category, shared by several types of machines
providing different services like "xrootd", "elog", etc. and it takes care
of installing common packages, repositories and configurations in all these
machines.
The SI machines use 2 different hostgroups "vocms" and "vocmsglidein". Only
the crab schedds use the "vocmsglidein" hostgroup, the rest uses "vocms".
The "vocms" hostgroup is also shared by other services like "elog", "xrootd",
"wmarchive", etc so it is important to be careful when modifying its code
and actually we do not ususally do it, this is under the responsibility of
the VOC and  we normally only modify the subhostgroups that are used by our 
infrastructure.

    
## Subhostgroup

This is a category within a hostgroup that applies more
specific configurations. It is important to understand that there could be a
subhostgroup inside another subhostgroup that in turn it is used to apply 
even more specific configurations.

The SI machines are classified as follows:

* Frontend: vocms/glideinwms/gwmsfrontend. 
* Central manager: vocms/si_htcondor/central_manager
* CCB: vocms/si_htcondor/ccb
* Production schedd: vocms/production/schedd
* Tier0 schedd: vocms/tier0/schedd
* Analysis schedd: vocmsglidein/crab

For more information about each of the profiles above, go to the puppet
subsection [subhostgroups](./subhostgroups.md)

Notice how each of the different profiles are built from a hostgroup, 
followed by one or more subhostgroups separated by a "/". In the case
of the "Frontend" profile you can notice that "glideinwms" is a subcategory
of "vocms" and "gwmsfrontend" is a subcategory of "glideinwms".

If you ever need to know what hostgroup/subhostgroup(s) is a machine assigned
you can consult [foreman](https://judy.cern.ch)(judy.cern.ch) and look for
a specific hostname. One also can look for a hosgroup/subhostgroup(s) and
see the set of machines assigned to it. Notice that this service only work
inside cern. A nice trick to use this from outside is to use W3M from a 
computer in cern.


## Modules

A module is a set of configurations to setup a specific service
and can be used within the different subhostgroups. There are 2 modules
used by the SI machines "vocmshtcondor" and "vocmsgwmsfrontend":

* **vocmshtcondor:** This module is in charge of creating a proper
    environment for the condor installation as well as properly configuring
    condor depending on the profile of the machine executing the module
    and the "**hiera variables**" in the ".yaml file" of the node in
    question.

* **vocmsgwmsfrontend:** Similarily to the "vocmshtcondor", this module is
    in charge of creating a proper environment to install a glideinWMS
    Frontend.

A deeper documentation of these modules can be found in te puppet
subsection: [modules](./modules.md)
    

## Hiera Variables

Each machine has its own .yaml file where it sets certain
variables to configure the modules above. Depending on these variables the
modules or subhostgroups will act differently, for example, the
"condor_pool" variable makes the vocmshtcondor module to install one set
or another of the condor configuration files.

These variables can be grouped in two categories:

1. Used on the "vocmshtcondor" module and in all profiles but the 
    Frontend(vocms/glideinwms/gwmsfrontend):
    * vocmshtcondor::condor_version
    * vocmshtcondor::condor_pool
    * vocmshtcondor::condor_admin
    * vocmshtcondor::prod_jobrouter
    * vocmshtcondor::ramdisk_spool
    * data_mount

2. Used on the vocmsgwmsfrontend module and in the Frontend 
    profile(vocms/glideinwms/gwmsfrontend):
    * vocmsgwmsfrontend::gwms_version
    * vocmsgwmsfrontend::htcondor_version
    * vocmsgwmsfrontend::gwms_pool
    * vocmsgwmsfrontend::gwms_admin
    * vocmsgwmsfrontend::data_mount
    * vocmsgwmsfrontend::gwms_service_cert
    * vocmsgwmsfrontend::gwms_pilot_cert

For more information about the hiera variables go to the puppet subsection:
[hiera variables](./hiera_variables.md)

If you want to know what is the value of a hiera variable for a specific
host you can execute the following command from aiadm:

```ai-hiera -n [HOSTNAME] [HIERA_VARIABLE]```  

e.g. ```ai-hiera -n vocms0250 vocmshtcondor::prod_jobrouter```  

