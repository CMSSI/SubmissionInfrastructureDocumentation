# Emulated pilot

[THIS IS A WORK IN PROGRESS]

This project aims to emulate a glideinWMS pilot. Initially it was thought to be used for debuging purposes but it can also
be used to validatea Worker node prior to use with CMS production jobs.

It is important to understand that even when the glidein is emulated, it still need to define most of the caracteristics of a normal
 pilot like the factory thatlaunched it, the forntend that requested it, its entry, etc. 


## The repository

This is the repository for this project: [gitlab.cern.ch/CMSSI/cms_emulated_glidein](https://gitlab.cern.ch/CMSSI/cms_emulated_glidein)
and a brief decription of the files in there:
 * **start.sh.** The main script, it will define most of the glidein configuration and launch it. A longer description 
    can be found in the next section.
 * **glidein_startup.** This script is coming from the glideinWMS Factory, it is the main executable of the glideins running on the
    grid..
 * **setenv.source.** Defines the proxy location and the working environment of the emulated glidein.
 * **get_factory_checksums.sh.**It is used to get checksum hash from the Factory.
 * **get_frontend_checksums.sh.**It is used to get checksum hash from the Frontend.
 * **99-htcondor.conf.**. This file can be copied (if needed) into /etc/security/limits.d to make sure the "nofile" limits for 
    condor are not set to "unlimited" which would trigger a bug in CentOS 7 and fail the execution of the "start.sh" script



## The start.sh script
As stated before, this is the main script, the one that will launch the pilot. Once you make sure all the paramenters inside this
file are correct, executing this script as root will start the pilot.

It is composed by 4 different sections: 

 1. General configs
 2. Factory and Frontend checksums
 3. Other parameters
 4. Execution

#### General configs
On this section the configuration parameters of the pool, in which the pilot will be emulated, are set:

The following parameters are factory-related:

 * FACTORY. The name of the factory where the pilot will download its factory-related scripts. The name should be the same as the
    one defined by the factory in its main configuration file normally at /etc/gwms-factory/glideinWMS.xml. e.g.:
     ```<glidein ... factory_name="CERN-ITB" ...```

 * ENTRY. The Entry Name of the pilot. At the moment, this has only been used for debugging purposses in which case a random entry
     name has been picked without causing too much trouble. In the future special Entries should be set for this purpose.
 * SCHEDD. The schedd name on the factory where the pilots is supposed to come from
 * WEB. A web directory on the factory where the glideinWMS files, used by th epilot, are stored
 * PARAM_GLIDECLIENT_REQUNODE. The hostname of the factory. Notice that a '.' n the hostname gets translated to the string ".dot,"
 
 
The following parameters are frontend-related:

 * CLIENTGROUP. The name of the frontend group, as stated in the frontend configuration file at /etc/gwms-frontend/fronted.xml e.g.: 
    ``` <group name="CERN_CAF" ..."> ```
 * CLIENTNAME. Frontend name as specified in the frontend configuration file at /etc/gwms-frontend/frontend.xml e.g.:
    ```<frontend ... frontend_name="CMSG-ITB_gWMSFrontend-v1_0"``` plus the frontend group name (same as CLIENTGROUP)
 * PARAM_GLIDEIN_COLLECTOR. The hostname of the pool (user collector) where the pilot will connect to, including the port
    range where the secondary collectors are listening. Notice that a '.' n the hostname gets translated to the string ".dot,"


#### Factory and Frontend checksums

When a pilot starts (emulated or not) it downloads a buch of files (mostly validation scripts) from both the factory and the 
 frontend. To make sure the pilot gets the right version of these files and that the files are not corrupted some checksums are used.

Factory-related parameters:

 * DESCRIPT. Points to the file that contains the list of checksums of the set of files to be downloaded and that are generic for
    all of the entries
 * SIGN. This is the checksum of the file above.
 * SIGNENTRY. This is the checksum of the file containing the checksums of the files to be downloaded specifically for the given 
    entry

Frontend-related parameters:

 * CLIENTDESCRIPT. Points to the file that contains the list of checksums of the set of files to be downloaded and that are generic
     for all of the different groups in the frontend
 * CLIENTSIGN. This is the checksum of the file above
 * CLIENTSIGNGROUP. This is the checksum of the file containing the checksums of the files to be downloaded specifically for the
     given frontend group

Normally this checksum is provided by the Factory when the pilot is sent to the site but in the case of an emulated glidein this 
 never happens so we need to get these checksums somehow. The scripts **get_frontend_checksums.sh** and **get_factory_checksums.sh** 
 are used for that. These scripts need to be executed within the respectively frontend and factory and passing as an argument the
 entry name and the frontend group to be used e.g.:

```get_factory_checksums.sh CMSHTPC_T2_CH_CERN_ce511 ```

``` get_frontend_checksums test```

#### Other parameters

Almost at the bottom of the start.sh script you can see the list of arguments that are passed to the glidein_startup.sh script. Most
 of these parameters were already described above, but there are others that were not and they have hardcoded values. 
These parameters come mainly from:
 * Frontend configuration file (/etc/gwms-frontend/frontend.xml) under the "attrs" tag (only those which define "parameter=True").Notice
 that the "attrs" tag can be defined both in the general and the group section of the fronted configuration.
 * Factory configuration file (/etc/gwms-factory/glideinWMS.xml) under the "attrs" tag
 * Entry configuration file (/etc/gwms-factory/config.d/) under the "attrs" tag

An example of the configuration described above is:
``` 
   <attrs>
      <attr name="CMS_GLIDEIN_VERSION" glidein_publish="True" job_publish="True" parameter="True" type="int" value="16"/>
      <attr name="GLIDEIN_Max_Idle" glidein_publish="True" job_publish="True" parameter="True" type="int" value="600"/>
   </attrs>
```

At the moment, there si not an automatic way to get these parameters but one way to get them is the following:

 1. Make the factory to request pilots to the desired entry
 2. Query (condor_q) the schedds in the factory for that specific entry and frontend
 3. Use the "-wide" option in condor_q to see all the parameters and values

```
sudo condor_q -g -const 'owner == "fecmsglobal" && GlideinEntryName=="CMSHTPC_T2_US_Vanderbilt_ce3_whole"' -limit 1 -wide
```

```
-- Schedd: schedd_glideins3@vocms0206.cern.ch : <137.138.52.148:22129?... @ 12/14/18 16:21:57
 ID        OWNER            SUBMITTED     RUN_TIME ST PRI SIZE CMD
271313.0   fecmsglobal    12/13 15:24   1+00:02:09 R  0    0.1 glidein_startup.sh -v std -name gfactory_instance -entry CMSHTPC_T2_US_Vanderbilt_ce3_whole -clientname CMSG-v1_0.main -schedd schedd_glideins3@vocms0206.cern.ch -proxy None -factory CERN-Prod -web http://vocms0206.cern.ch/factory/stage -sign c373239bcd5db9dc43ecafdbe874e87c880cc9b0 -signentry 3cb26ff622b63207071ec656211cfc3e1403f561 -signtype sha1 -descript description.icdbRM.cfg -descriptentry description.icdbRM.cfg -dir OSG -param_GLIDEIN_Client CMSG-v1_0.main -submitcredid 411868 -slotslayout fixed -clientweb http://vocms080.cern.ch/vofrontend/stage -clientsign f0220ec9599cd8d0b438bb0624b76735175524c9 -clientsigntype sha1 -clientdescript description.icdaKN.cfg -clientgroup main -clientwebgroup http://vocms080.cern.ch/vofrontend/stage/group_main -clientsigngroup 74b21918f8581f41bbe261725fce5415654b4c00 -clientdescriptgroup description.icdaKN.cfg -param_CONDOR_VERSION 8.dot,6.dot,3 -param_GLIDEIN_Glexec_Use OPTIONAL -param_CMS_GLIDEIN_VERSION 16 -param_GLIDEIN_Job_Max_Time 14400 -param_GLIDECLIENT_ReqNode vocms0206.dot,cern.dot,ch -param_GLIDEIN_CLAIM_WORKLIFE_DYNAMIC cpus.star,.open,6.star,3600.close, -param_USE_PSS True -param_MEMORY_USAGE_METRIC ifthenelse.open,ProportionalSetSizeKb.eq,.question,.eq,UNDEFINED.comma,.open,ResidentSetSize.nbsp,.plus,1023.close,/1024.comma,.open,ProportionalSetSizeKb.nbsp,.plus,.nbsp,1023.close,/1024.close, -param_GLIDEIN_CCB vocms0806.dot,cern.dot,ch.colon,9619.minus,9720.semicolon,cmssrv258.dot,fnal.dot,gov.colon,9619.minus,9720 -param_GLIDEIN_Max_Idle 600 -param_GLIDEIN_Monitoring_Enabled False -param_CONDOR_ARCH default -param_UPDATE_COLLECTOR_WITH_TCP True -param_MIN_DISK_GBS 1 -param_GLIDEIN_Max_Tail 600 -param_USE_MATCH_AUTH True -param_CONDOR_OS default -param_GLIDEIN_Report_Failed NEVER -param_GLIDEIN_Collector cmssrv221.dot,fnal.dot,gov.colon,9621.minus,9720.semicolon,vocms0815.dot,cern.dot,ch.colon,9621.minus,9720 -cluster 271313 -subcluster 0
```

#### Execution

The last line of the start.sh script executes a command as the user "condor" and that command is nothing but the source of the 
setenv.source file and the glidein_startup.sh script.


The following is a checklist that need to be completed before launching an emulated pilot:

 * The "condor" user exists, even if not a condor installation exists
 * The workernode has acccess to cvmfs
 * You can create a proxy with a DN whitelisted in the Central Manager of the pool you wish to connect to
 * The proxy has an expiration date of at leat 12 hours in the future
 * The owner of the proxy file is the user "condor" (you can create a proxy with one username and change its owner with chown)
 * All the parameters described in "The start.sh script" section are valid
 * Within the setenv.source file, the _CONDOR_SCRATCH_DIR envar points to an existing directory owned by the condor user
 * Within the setnev.source file, the X509_USER_PROXY points to the proxy file described above
 * If in CentOS 7, make sure the nofile limit for condor is not set to "unlimited" (/etc/security/limits.d/). This triggers
  a bug that doesn't let you to execute commands as the "condor" user
 * If the selected entry is supposed ot have singularity( GLIDEIN_REQUIRED_OS="any") the node needs to have singularity 
 installed





