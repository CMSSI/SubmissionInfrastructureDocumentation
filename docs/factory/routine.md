
## Upgrade GlideinWMS Factory & Condor

**Upgrade GlideinWMS Factory**

```
sudo systemctl stop gwms-factory
sudo yum upgrade glidein*
sudo gwms-factory upgrade
sudo gwms-factory reconfig
sudo systemctl start gwms-factory
```
If you need to upgrade the factory from the OSG development repository, replace ```sudo yum upgrade glidein*``` with the following command:
```
sudo yum --enablerepo=osg-development upgrade glidein*
```
In order to remove idle pilot jobs, run this command between stopping and restarting the factory:
```
for i in {1..10}; do sudo -u gfactory condor_rm -name schedd_glideins$i@hostname -const 'jobstatus==1'; done
```


**Upgrade Condor**

```
sudo systemctl stop gwms-factory
sudo yum upgrade condor*
##After 3 min
sudo systemctl start gwms-factory 
```

##  Operate Machines by Openstack

**Change Openstack Project**

In terminal, first log in to aiadm.cern.ch, there are plenty of tools to manage machines.

```
eval $(ai-rc "CMS Factory")
```

**Check List of the Machines**

```
openstack server list
```

**Check List of the Volumes**

```
openstack volume list
```

**Create Volume**

You can find more details, such as possible types, on [CloudDocs](https://clouddocs.web.cern.ch/details/block_volumes.html). Size is defined in GB.
```
openstack volume create --type io1 --size XX volumeName
```

**Check Server Details**

This command will list down all properties associated with machine, such as hostgroup, LandDBSets, etc..

```
openstack server show __Hostname__
```

**Reboot server**

There are two types of the reboot, soft and hard reboot

```
openstack server reboot (--soft OR --hard) Hostname
```

**Enable IPv6 on Machine**

```
openstack server set --property IPv6-ready="True" Hostname
```

## Operate Machines by AIADM Tools

In general there are two types of the resources, Puppet managed and Non Puppet managed. Puppet managed machines handle by aiadm tools, like kill the machine, launch a VM in hostgroup, check hiera data associated with VM etc...

**Check machine details**

```
ai-dump Hostname
```

**Check Machine hiera data**

There two major hiera data structure **Array** & **hash**, following options define type of the look up

**hash**: -s

**array**: -a

```
ai-hiera -s -n Hostname Hiera-Key
```

**Kill Machine**

```
ai-kill Hostname
```

##  Launch New Factory

1. Create a YAML file and check it in the hostgroup Gitlab, convention for YAML file is **Hostname.yaml** e.g. vocms0204.cern.ch.yaml

2. Copy one of the existing YAML to new one and modify it accordingly.
    * Factory Name
    * GlidinWMS factory version 

3. Create a volume in Factory Openstack project with High I/O type (explained in the previous section). Note the volume ID as it will be used to attach the volume to the instance in step 6. 

4. If different from default, choose an OS image and/or flavor to apply. To list the available options,  use the following command:
    ```
    openstack <option> list
    ```

    where \<option\> can be 'image' or 'flavor'.

5. Check available machine names and choose one:

        for i in `seq 10 400`; do nslookup vocms0$i | grep NXDOMAIN ; done;

6. The following command creates a new factory instance. Specify parameters (image, flavor, volume ID, name) you defined in steps 3-5. If you're replicating an existing machine, refer to its details on Openstack or using `ai-dump vocmsXXXX`. 
*Note: the name 'vocms0204' and its parameters are used as an example.*

        ai-bs --foreman-hostgroup vocmsfactory/service --foreman-environment qa --landb-mainuser='cms-service-glideinwms-factory' --landb-responsible='cms-service-glideinwms-factory' --nova-image 2b557281-c809-47c0-9745-34a2a9e68137 --nova-availabilityzone cern-geneva-b --nova-flavor m2.large --nova-sshkey lxplus --nova-attach-existing-volume vocms0204=35949546-e703-4c7b-8d63-ca5791cfae50 vocms0204

## Post Configuration

Factory machine creation is fully puppetized, but during installation **glideinwms-factory** & **condor** installation files move to volume to allocate more space, during this procedure puppet does weird things on permission even all defined to not to. To avoid that, following procedures are required:

```
yum remove glideinwms* condor* -y
rm -rf /data/*
rm -rf /etc/gwms-factory /etc/condor /var/lib/condor /var/lib/gwms*
```

After only needs to run puppet again to take care of everything.

**First**, start condor

```
sudo systemctl start condor
```

**Second** upgrade & start factory

```
sudo gwms-factory upgrade
sudo systemctl start gwms-factory
```

## Factory commands

**Reconfigure Factory**

```
sudo systemctl stop gwms-factory
```

It is important to change directory to shared config files and pull changes before re-configuring service.

**Shared config files**: /etc/osg-factory

```
sudo gwms-factory reconfig
sudo systemctl start gwms-factory 
```

**Put Entry in Downtime**

```
sudo gwms-factory down -entry (Entry Name) -start (Start date) -end (End of downtime)
```

**Take out Entry from Downtime**

```
sudo gwms-factory up -entry (Entry Name) -start (Start date) -end (End of downtime)
```

## Testing a new entry using CRAB

Get your [grid certificate](https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideRunningGridPrerequisites#Grid_certificate).

Log in to lxplus, create a directory for the tests, and create the CMSSW project. You only need to do this the FIRST TIME.

```
mkdir entry_tests
cd entry_tests
cmsrel CMSSW_7_3_5_patch2
cp /afs/cern.ch/user/m/mmascher/public/entry_testing/* ~/entry_tests
```

Now load the environment and sent the CRAB jobs!

```
cd ~/entry_tests
source env.sh
crab submit crabFactOps.py # Please open this file and change the site as necessary!
crab status #you can add a project dir to check a specific CRAB task
```

If you want to learn more about CRAB, please visit [CRAB Tutorial](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookCRAB3Tutorial).

## Removing limits from factory entries

Traditionally factory operators used to maintain limits in the factory entries. For example:

```
            <max_jobs>
               <default_per_frontend glideins="50" held="5" idle="10"/>
               <per_entry glideins="100" held="10" idle="40"/>
            </max_jobs>
```

In order to decrease the operational effort needed to micro manage those limits it was decided to start using "infinite" limits in the factory, and leave the decisions entirely to the frotnend. Starting [October 2020](https://cms-logbook.cern.ch/elog/GlideInWMS/7488) Factory Operations began removing factory limits to the CMS entries. Here is an explanation of what it has been done:

* All sites that have CMS in supportedVO were considered
* Progressively, starting from T1, then US T2, then all T1 + T2, and finally T3 were all migrated to limitless entries
* A query to the factory collector to get entry name from the site (GLIDEIN_CMSSite) was performed (only at CERN factory)
* Exception are the following sites who do not belong to the main group in the CMS frontend: `!stringListsIntersect("T3_US_VC3_NotreDame,T3_US_OSG,T3_US_TAMU,T3_US_FNALLPC,T3_US_SDSC,T2_US_Nebraska,T2_US_Caltech",GLIDEIN_CMSSite) && stringListsIntersect("CMSHTPC_T1_IT_CNAF_CINECA,CMSHTPC_T1_DE_KIT_cloud-htcondor-ce-1-kit,CMSHTPC_T1_DE_KIT_cloud-htcondor-ce-1-kit-short",EntryName)`
* Some sites asked to revert the change:
    * T2_AT_Vienna (only allows 95 pilots).See [here](https://ggus.eu/index.php?mode=ticket_info&ticket_id=151340)
    * T2_FR_GRIF_LLR (issues with too many idle). See [here](https://ggus.eu/index.php?mode=ticket_info&ticket_id=152574)
    * T2_KR_KISTI (held, no admin interaction)
    * T2_US_Vanderbilt (issues with too many idle). See [here](https://github.com/opensciencegrid/osg-gfactory/commit/2e42e077be2bbaafbe0723ebf105cb333820f4c8#commitcomment-49511720)
    * T3_KR_KNU (held, no admin interaction)
    * T3_US_Baylor (held, no admin interaction)
    * T3_US_UMD (only allows 125 pilots). Email "[Osg-gfactory-support] cmspilot jobs at University of Maryland UMD T3 site".

## ARC GAHP

GAHP is the process in the factory that talks to the CE. It is used by Condor's GridManager to submit jobs, query status, etc, all of which is logged in `/dev/shm/GridManagerLog.schedd_glideins*` files.
All ARC CEs are served by a single GAHP process, arc_gahp. 

This has in the past resulted in a chain of issues where the ARC GAHP fails due to a single broken CE and then is unable to process all the following sites. To remedy this, follow the steps in the section [Detailed logging for a CE](https://cmssi.docs.cern.ch/factory/routine/#detailed-logging-for-a-ce) to isolate the troubled CE in its own ARC GAHP and enable extra logging to identify the issue.

### arc_gahp test
To test connection to an ARC CE, you can run a standalone `arc_gahp` process. It allows to replicate the commands Condor runs to submit jobs and query the jobs on the CE. 
This requires the x509 proxy and the CE hostname, which you can obtain using the following: 
```
$ entry_q <entry name> -all [-limit 1] -af x509userproxy gridresource
```
Alternatively, print all pilot details by replacing `-af <arg≥` with `-l`. 
Then, as `gfactory`, use the following commands:
```
$ arc_gahp
  INITIALIZE_FROM_FILE <x509 proxy file>

  ARC_JOB_STATUS_ALL 3 <CE hostname followed by a space>

  RESULTS

  ARC_JOB_INFO 3 <CE hostname> <job ID from RESULTS output>

  RESULTS 
```

> 🚨
>
>Make sure to leave a **space after the hostname** in `ARC_JOB_STATUS_ALL 3 ce.hostname.org `!

To use a token instead of proxy:
```
UPDATE_TOKEN /home/gfactory/test_arctoken/submit/token_itb
```
To enable debug logging:
```
$ arc_gahp -d 0
```
Logs can be found in `/tmp/ArcGahpLog.<user>`.


Full example:
```
[gfactory@vocms0204$ arc_gahp -d 0
$GahpVersion 0.1 Mar 16 2021 Condor\ ARC\ CE\ GAHP $
INITIALIZE_FROM_FILE /var/lib/gwms-factory/client-proxies/user_fecmsglobalitb/glidein_gfactory_instance/credential_CMSG-ITB_gWMSFrontend-v1_0.main-arc_MXV7NJMJ
S
ARC_JOB_STATUS_ALL 3 sbgce1.in2p3.fr
S
RESULTS
S 1
3 201 OK 1 MRaMDmGzdX6nEmaJ7oEZGSWqeJPcRmABFKDmsMzQDmEEFKDm1lWSUn ACCEPTED
ARC_JOB_INFO 3 sbgce1.in2p3.fr MRaMDmGzdX6nEmaJ7oEZGSWqeJPcRmABFKDmsMzQDmEEFKDm1lWSUn
S
RESULTS
S 1
3 499 Invalid\ response\ (no\ ComputingActivity\ element)
```


## Detailed logging for a CE

Extra logging between factory and a CE can be enabled in the local Condor configuration file, `/etc/condor/config.d/99_local_tweaks.config`*. The config contains these lines:
```
#GRIDMANAGER_LOG_APPEND_SELECTION_EXPR = True
#GRIDMANAGER_SELECTION_EXPR = regexps("([^ ]*) .*",GridResource,"\1")
```
Where
- `GRIDMANAGER_SELECTION_EXPR` starts a separate gridmanager daemon, or GAHP, for a set resource
- `GRIDMANAGER_LOG_APPEND_SELECTION_EXPR` creates a separate log file for the gridmanager daemon defined in the selection expr.  
You can read more about the macros in [HTCondor docs](https://htcondor.readthedocs.io/en/lts/admin-manual/configuration-macros.html#GRIDMANAGER_SELECTION_EXPR).


#### Steps to enable extra logging for a CE
1. In the `99_local_tweaks.config` file, uncomment lines
```
GRIDMANAGER_LOG_APPEND_SELECTION_EXPR = True
GRIDMANAGER_SELECTION_EXPR = regexps("([^ ]*) .*",GridResource,"\1")
```
2. Change the expression to include the CE hostname, e.g. `ce02.cmsaf.mit.edu`:
```
GRIDMANAGER_SELECTION_EXPR = regexps("(([^ ]*) (ce02.cmsaf.mit.edu)?)",GridResource,"\1")
``` 
3. Use pipe to add another CE to the list, e.g. `(ce02.cmsaf.mit.edu|ce2.osg-htc.org)`:
```
GRIDMANAGER_SELECTION_EXPR = regexps("(([^ ]*) (ce02.cmsaf.mit.edu|ce2.osg-htc.org)?)",GridResource,"\1")
```
4. Save local tweaks changes, then reconfigure condor:
```
sudo condor_reconfig
```

The corresponding GAHP log file will appear in `/dev/shm/`. The name of the file, starting with `GridManagerLog.schedd_glideins*`, should end with the hostname of the CE you specified in the config, e.g.:
```
tail -f /dev/shm/GridManagerLog.schedd_glideins4.gfactory.condor_ce02.cmsaf.mit.edu
```

Please note that enabling this setting will result in detailed logs, and as such, take up a lot of space. Don't forget to disable extra logging for a CE once you're finished. 


*The `99_local_tweaks.config` file is in the `vocmsfactory` hostgroup reopository. The file is set to not be overwritten once it's created, so you can make changes locally on the machine.


## Testing authentication of an entry
Authentication of CE can be checked with the `condor_ce_ping` and `condor_ce_trace` commands. You can force these commands to use scitoken authentication by setting the variable `_condor_SEC_CLIENT_AUTHENTICATION_METHODS` to `SCITOKEN`, and `BEARER_TOKEN_FILE` to the token file you want to use. 

1. Find a scitoken of a reliable CE:
```
entry_q CMSHTPC_T2_DE_DESY_grid-htcondorce0 -all -limit 1 -af scitokensfile
```
Check the validity of the token by copying the contents and pasting them in [jwt.io](https://jwt.io/). 
Inspect the expiry date and that it has the correct fields (currently tokens are not specialized per site, i.e. the "aud" field has "any". This might change in the future).

2. (As user 'gfactory' going forward) Copy the scitoken to `/tmp/token` and set `BEARER_TOKEN_FILE` to point to it:
    ```
    $ cp /var/lib/gwms-factory/client-proxies/user_fecmsglobal/glidein_gfactory_instance/credential_CMSG-v1_0.main_CMSHTPC_T2_DE_DESY_grid-htcondorce0.scitoken /tmp/token

    $ export BEARER_TOKEN_FILE=/tmp/token
    ```
3. Use the test commands with the CE hostname to test the authentication. Extra debugging is enabled using  flag `-d` and parameter `_condor_TOOL_DEBUG=D_FULLDEBUG,D_SECURITY`.
    -  `condor_ce_ping` - needs name (CE hostname), pool (CE hostname with collector port, usually 9619). E.g.:
        ```
        $_condor_SEC_CLIENT_AUTHENTICATION_METHODS=SCITOKEN _condor_TOOL_DEBUG=D_FULLDEBUG,D_SECURITY condor_ce_ping -pool ce5-vanderbilt.sites.osg.org:9619 -name ce5-vanderbilt.sites.osg.org WRITE -d
        ```
        If successful, you can expect a similar message:
        ```
        WRITE command using (AES, AES, and SCITOKENS) succeeded as cmspilot@users.htcondor.org to schedd ce5-vanderbilt.sites.osg.org.
        ```
    - `condor_ce_trace` - will try to run a test job. Needs only the CE hostname. E.g.:
        ```
        _condor_SEC_CLIENT_AUTHENTICATION_METHODS=SCITOKEN _condor_TOOL_DEBUG=D_FULLDEBUG,D_SECURITY condor_ce_trace ce5-vanderbilt.sites.osg.org WRITE -d 
        ```
Successful tests indicate that the CE is reachable and can authenticate using a functioning scitoken.  

## Removing tarballs when running out of disk space 
Tarballs accumulate with every reconfig and can end up occupying a significant amount of disk space. If you notice the machine running out of space via alerts or failing reconfigs (can verify this by running `htop` in parallel during reconfig), use the following steps.

1. Navigate to the location for tarball removal:
```
$ cd /data/lib/gwms-factory/web-area
```
2. Find and remove tarballs older than 300 days:
```
$ find . -iname "condor_bin*" -atime +300 -type f | xargs sudo rm
```