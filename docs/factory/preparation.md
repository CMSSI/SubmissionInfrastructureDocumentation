## E-groups

 CMS GlideinWMS Factory operator has to register to the below [e-groups](https://e-groups.cern.ch/e-groups/EgroupsSearchForm.do) list:

* ai-admins (access to the configuration management system)
* LxAdm-Authorized-Users (access to aiadm.cern.ch administrative nodes)
* cms-factory-operators (CMS GlideinWMS Factory operators)

 Once you subscribe to cms-factory-operators e-group, you automatically become a members of these e-groups:

* cms-gfactory-alerts (alerts from factory monitoring)
* cms-htcondor-admins (alerts from htcondor)
* cms-service-factory-admins (access to it-puppet-hostgroup-vocmsfactory repository)
* cms-service-glideinwms-admins (access to all production and testing Submission Infrastructure Nodes)
* cms-service-glideinwms-factory (list for CMS GlideinWMS Factory operations)

## Two-Factor Authentication

Set up [Two-Factor Authentication](https://security.web.cern.ch/recommendations/en/2FA.shtml). CERN sensitive services will require you to make use of an authenticator app on your SmartPhone or a YubiKey.

## CMS Talk

Subscribe to the following [CMS Talk](https://cms-talk.web.cern.ch/c/offcomp/si/161) forums: O&C -> Submission Infrastructure.

## Grid User Certificate

Get [Grid User certificate](https://ca.cern.ch/ca/user/Request.aspx?template=ee2user) and add it to your browser.

## GGUS

Request [your vo membership for CMS VO](https://voms2.cern.ch:8443/voms/cms/user/home.action). When Voms-Admin approve your request, then you need to request membership for `/cms/TEAM`. After some time, a new account will be created via VOMS syncronization in the [GGUS](https://ggus.eu/).

## Globus Credentials

Create directory where your credentials will be kept:

```
ssh lxplus
mkdir .globus
```

Copy your [Grid User certificate](https://ca.cern.ch/ca/user/Request.aspx?template=ee2user) into the .globus directory. Run the following commands in .globus directory in order to generate your certificate and private key:

```
openssl pkcs12 -in myCertificate.p12 -out usercert.pem -clcerts -nokeys
openssl pkcs12 -in myCertificate.p12 -out userkey.pem -nocerts
```

Set the appropriate permissions for the generated certificates:

```
chmod 400 userkey.pem
chmod 600 usercert.pem 
```

When Voms-Admin approve [your vo membership for VO cms](https://voms2.cern.ch:8443/voms/cms/user/home.action) request, then you can generate a proxy:

```
voms-proxy-init -voms cms 
```

## OSG settings

Ask Jeffrey Dost to be added to osg-gfactory-support and other OSG Factory operations lists. Also, ask him to create new Jira account.

## Bridge machine at UCSD 

Send Terrence Martin your user name and public ssh key in order to create an account on uaf-10.t2.ucsd.edu:

```
ssh lxplus
ssh-keygen -t rsa -b 4096-C "your.email@cern.ch"
cd ~/.ssh
less id_rsa.pub
```

When you have an access to uaf-10.t2.ucsd.edu, then place here your private and public key:

```
ssh lxplus
cd ~/.ssh
scp id_rsa uaf-10.t2.ucsd.edu:~/.ssh/id_rsa
scp id_rsa.pub uaf-10.t2.ucsd.edu:~/.ssh/id_rsa.pub
```

## Meetings

You are supposed to attend the following meetings:

* [Submission Infrastructure Meeting](https://indico.cern.ch/event/1183352/) on Thursdays at 17:00 CERN time.
* [Factory Operations Meeting](https://ucsd.zoom.us/j/96342708512?pwd=TWRxL0JTaXIxVEc1d2ovaWprYi9sUT09#success) on Fridays at 18:00 CERN time.

## Report

Send weekly activity report to Dave Dykstra and L2 Coordinators of Submission Infrastructure.

## Repositories

* [GitLab vocmsfactory host group](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmsfactory)
* [Entry configuration](https://github.com/opensciencegrid/osg-gfactory)
* [Factools](https://github.com/jdost321/factools)
* [CMS Frontend validation scripts](https://gitlab.cern.ch/CMSSI/CMSglideinWMSValidation/-/tree/production?ref_type=heads)
* [Factory MONIT](https://gitlab.cern.ch/CMSSI/SubmissionInfrastructureMonitoring/-/tree/master/monit/factory)
* [GlideinWMS](https://github.com/glideinWMS/glideinwms)
* [OSG Topology](https://github.com/opensciencegrid/topology)

## Tickets
Here you can find requests for you to work on:

* [CMS Glidein Factory GGUS](https://ggus.eu/index.php?mode=ticket_search&su_hierarchy=0&status=open&cms_su=CMS+Glidein+Factory&date_type=creation+date&tf_radio=1&timeframe=any&from_date=21+May+2020&to_date=22+May+2020&ticket_category=all&typeofproblem=all&specattrib=none&orderticketsby=REQUEST_ID&orderhow=desc&ticket_per_page=50&show_columns_check%5B0%5D=TICKET_TYPE&show_columns_check%5B1%5D=AFFECTED_VO&show_columns_check%5B2%5D=AFFECTED_SITE&show_columns_check%5B3%5D=PRIORITY&show_columns_check%5B4%5D=RESPONSIBLE_UNIT&show_columns_check%5B5%5D=CMS_SU&show_columns_check%5B6%5D=CMS_SITE&show_columns_check%5B7%5D=STATUS&show_columns_check%5B8%5D=DATE_OF_CHANGE&show_columns_check%5B9%5D=SHORT_DESCRIPTION&show_columns_check%5B10%5D=SCOPE&search_submit=Search)
* [CMS Submission Infrastructure GGUS](https://ggus.eu/index.php?mode=ticket_search&su_hierarchy=0&status=open&cms_su=CMS+Submission+Infrastructure&date_type=creation+date&tf_radio=1&timeframe=any&from_date=21+May+2020&to_date=22+May+2020&ticket_category=all&typeofproblem=all&specattrib=none&orderticketsby=REQUEST_ID&orderhow=desc&ticket_per_page=50&show_columns_check%5B0%5D=TICKET_TYPE&show_columns_check%5B1%5D=AFFECTED_VO&show_columns_check%5B2%5D=AFFECTED_SITE&show_columns_check%5B3%5D=PRIORITY&show_columns_check%5B4%5D=RESPONSIBLE_UNIT&show_columns_check%5B5%5D=CMS_SU&show_columns_check%5B6%5D=CMS_SITE&show_columns_check%5B7%5D=STATUS&show_columns_check%5B8%5D=DATE_OF_CHANGE&show_columns_check%5B9%5D=SHORT_DESCRIPTION&show_columns_check%5B10%5D=SCOPE&search_submit=Search)
* [CMS_Submission Infrastructure GGUS](https://ggus.eu/index.php?mode=ticket_search&su_hierarchy=0&status=open&date_type=creation+date&tf_radio=1&timeframe=any&from_date=21+May+2020&to_date=22+May+2020&ticket_category=all&typeofproblem=CMS_Submission+Infrastructure&specattrib=none&orderticketsby=REQUEST_ID&orderhow=desc&ticket_per_page=50&show_columns_check%5B0%5D=TICKET_TYPE&show_columns_check%5B1%5D=AFFECTED_VO&show_columns_check%5B2%5D=AFFECTED_SITE&show_columns_check%5B3%5D=PRIORITY&show_columns_check%5B4%5D=RESPONSIBLE_UNIT&show_columns_check%5B5%5D=CMS_SU&show_columns_check%5B6%5D=CMS_SITE&show_columns_check%5B7%5D=STATUS&show_columns_check%5B8%5D=DATE_OF_CHANGE&show_columns_check%5B9%5D=SHORT_DESCRIPTION&show_columns_check%5B10%5D=SCOPE&search_submit=Search)

## Links

URLs that needs to be bookmarked:

* Consoles:
    * [Foreman/Judy console](https://foreman.cern.ch/) (only accessible within CERN network)
    * [OpenStack console](https://openstack.cern.ch/)
* Configuration documents from CERN-IT from different departments, for cloud, monitoring, and configuration management:
    * [OpenStack Documentation](https://clouddocs.web.cern.ch/index.html)
    * [Configuration Management](https://configdocs.web.cern.ch/configdocs/)
    * [Configuration Training](https://configtraining.web.cern.ch/configtraining/)
    * [Monitoring Documentation](https://monit-docs.web.cern.ch/)
    * [Backup and Restore Service](https://backup.docs.cern.ch/)
* Factory documentation:
    * [Glidein Factory](http://www.t2.ucsd.edu/twiki2/bin/view/UCSDTier2/GlideinFactoryFAQ)
    * [GlideinWMS Factory Training](http://www.t2.ucsd.edu/twiki2/bin/view/UCSDTier2/GlideinFactoryTraining)
    * [GlideinWMS](https://glideinwms.fnal.gov/doc.prd/factory/index.html)
    * [Glideins in a vacuum](http://gfactory-itb-1.opensciencegrid.org/vacuum/)
* Home Pages Of Factories:
    * [CERN](http://vocms0207.cern.ch/factory/monitor/)
    * [CERN-ITB](http://vocms0205.cern.ch/factory/monitor/)
    * [CERN-ITB-DEV](http://vocms0203.cern.ch/factory/monitor/)
    * [FNAL](http://cmssi-factory02.fnal.gov:8319/monitor/)
    * [UCSD](http://gfactory-2.opensciencegrid.org/factory/monitor/)
    * [UCSD-ITB](http://gfactory-itb-1.opensciencegrid.org/factory/monitor/)
* Factory monitoring:
    * [Hosts monitor](https://monit-grafana.cern.ch/d/JI0ulEKMz/cms-submission-infrastructure-hosts-monitor?orgId=11&refresh=1m&from=now-3h&to=now&var-DS_PROMETHEUS=cmsmonitoring-victoriametrics&var-job=si&var-name=vocms0207.cern.ch&var-node=vocms0207&var-port=9100&var-service=gwmsfactory&var-type=prodfactory)
    * [Factory monitor](https://monit-grafana.cern.ch/d/ZRBP5iXZk/cms-submission-infrastructure-factories-monitor?orgId=11)
    * [Pilot logs](https://monit-grafana.cern.ch/d/MajRVJ8Mk/pilots-logs?orgId=11)
    * [Factory alerts](https://monit-grafana.cern.ch/d/AjhfpjtGk/factory-alerts?orgId=11)
    * [Pilot logs in OpenSearch](https://monit-timber.cern.ch/dashboards/app/discover#/?_g=(refreshInterval:(pause:!t,value:0),time:(from:now-4h,to:now))&_a=(columns:!(_source),filters:!(),index:ba7762b0-7503-11eb-a1e7-ff1aa71dbd34,interval:auto,query:(language:kuery,query:%27%27),sort:!(metadata.timestamp,desc)))
    * [Entry information in OpenSearch](https://monit-opensearch.cern.ch/dashboards/app/discover#/?_g=(filters:!(),refreshInterval:(pause:!t,value:0),time:(from:now-15m,to:now))&_a=(columns:!(_source),filters:!(),index:c215b4d0-aa66-11ea-b4af-1b9c890bd330,interval:auto,query:(language:kuery,query:%27%27),sort:!()))
    * [Pilot information in OpenSearch](https://monit-opensearch.cern.ch/dashboards/app/discover#/?_g=(filters:!(),refreshInterval:(pause:!t,value:0),time:(from:now-15m,to:now))&_a=(columns:!(_source),filters:!(),index:%2773e78400-aa66-11ea-b4af-1b9c890bd330%27,interval:auto,query:(language:kuery,query:%27%27),sort:!()))
* Monitoring links from CERN-IT, filter data that interests you:
    * [Host metrics](https://monit-grafana.cern.ch/d/000000116/host-metrics?orgId=1)
* Sites CEs:
    * [GOCDB](https://goc.egi.eu/)
    * [OSG Topology](https://github.com/opensciencegrid/topology/tree/master/topology)
    * [CMS CRIC](https://cms-cric.cern.ch/)
* Other helpful CERN links:
    * [General information on CERN computing services](https://cern.service-now.com/service-portal?id=services_department&dep=IT)
    * [CERN Certification Authority](https://ca.cern.ch/ca/)
    * [CERN network](https://network.cern.ch/)
    * [Service Status Board](https://cern.service-now.com/service-portal?id=service_status_board)
