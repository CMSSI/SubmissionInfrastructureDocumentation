<link rel="stylesheet" href="link/to/stylesheet" />
<style>
</style>

# Factories 

## CERN Machines

|  Name |Hostname|Hostgroup|Environment|Condor Version|GWMS Version|
|-------|--------|---------|-----------|--------------|------------|
|CERN-Prod|vocms0206.cern.ch|vocmsfactory/service|production|23.0.14-1|3.10.7-2|
|CERN-ITB|vocms0204.cern.ch|vocmsfactory/service|qa|23.7.2-1|3.10.7-2|
|CERN-ITB-Dev|vocms0202.cern.ch|vocmsfactory/service|qa|23.0.12-1|3.10.7-2|
|       |        |         |           |              |            |
|Monitoring|vocms0208.cern.ch|vocmsfactory/monitoring|production| |  | 

## UCSD/OSG

| Factory   | Hostname            | Role                          | Condor Version | GWMS Version |
| --------- | ------------------- | ----------------------------- | -------------- | ------------ |
| Tiger-Prod | factory-1.osg-htc.org  | Tiger Prod factory (Kubernetes)  |  23.9.6   |  3.10.7-2  |
