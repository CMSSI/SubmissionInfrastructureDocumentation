# Scale tests

Scale tests are performed regularly on the infrastructure to establish our
maximum operating ceiling and most importantly how far we are from hitting it.
i.e. do we have room to grow?


## Methodology

Scale tests are performed in the ITB pool which is a mirror replica of the
global pool, minus all the jobs. A number of schedds dedicated to scale testing
is spun up depending on the test requirements and a continuous stream of sleep
jobs is submitted through them. The state of the infrastructure is continuously
monitored during the tests to ensure the correct function and early detection of
problems.

Scale test schedds are created on demand, sample command is provided bellow but
it can be modified as needed.

```
export OS_PROJECT_NAME
$ ai-bs vocms0826.cern.ch --foreman-hostgroup vocmssi/schedd_htcondor/scale_test/itb --foreman-environment qa  --cc7 --nova-flavor m2.3xlarge
```
The schedds will be automatically initialized with all the required
configuration, additionally the `scale-test` executable from the [scale-test-scripts](https://gitlab.cern.ch/CMSSI/scale-test-scripts)
package can be used to fill up the schedd queue with jobs. The parameters of the
jobs are configured in the `/etc/scale-test-scripts/config.yml` configuration
file which is installed alongside the package. It is recommended that any
changes to the configuration be made on the package so that it is applied
automatically to all scale test schedds (more practical than manually updating
~10-20 machines).

Automation is setup to build and deploy the package as long as
all the usual steps to update RPM packages are taken, update the version or
revision in the spec file, add a changelog entry. On every push it will trigger
a test of the build process and the changes will automatically be published to
the internal cern repositories when there is a new git tag.

!!! danger
    It is strictly enforced that once a rpm version has been pushed it cannot be
    modified and further builds with that version will be rejected. Do not try
    to force-push tags if they have already been through the build process.

In order to keep the queues of the entire schedd cluster full the tool `wassh`
(Wide-area-ssh) can be used within the `aiadm` cluster.

```
$ wassh -c vocmssi/schedd_htcondor/scale_test/itb scale_test
```