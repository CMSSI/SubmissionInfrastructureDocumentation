# Failover

All machines in SI have a redundant backup located at Fermilab. The backup
machines are by default inert and monitor the status of the primary machines at
CERN. If, at any point, one machine goes down the Fermilab backup machine takes
over and ensures the smooth operation of the pools in order to avoid downtime.

For the htcondor central managers, at any given moment both machines are running and both collectors have the full set of resource classads. The active machine is the one that is running the negotiator daemons which perform the matchmaking between the jobs and the resource slots.

In any central manager you can query the negotiators that are running with the following command:
```
$ condor_status -nego
Name                           LastCycleEnd (Sec)   Slots Submitrs Schedds    Jobs Matches Rejections

NEGOTIATORT1@vocms0814.cern.ch   7/20 13:55    36   25953       58      27   76178     182        728
NEGOTIATORUS@vocms0814.cern.ch   7/20 13:57    48   40023       58      27   75886     218        737
vocms0814.cern.ch                7/20 13:59    42   27286       55      27   66911     677        765
```

All negotiators are running on the vocms0814 machine at CERN.

```
condor_off -subsystem -replication
condor_off -subsystem -had

condor_off -nego
# Repeat for all additional negotiators running on the node (Global pool runs NEGOTIATORT1 and NEGOTIATORUS as extra negotiators)
condor_on -daemon NEGOTIATORT1
condor_on -daemon NEGOTIATORUS
```

```
condor_on -daemon NEGOTIATORT1
condor_on -daemon NEGOTIATORUS
condor_on -nego
condor_on -subsystem -had
condor_on -subsystem -replication
```

In cases of a scheduled downtime of a machine it is recommended to switch to the backup preemptively to ensure there are no issues with the failover.