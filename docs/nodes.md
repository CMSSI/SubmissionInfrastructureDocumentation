<link rel="stylesheet" href="link/to/stylesheet" />
<style>
  @media only screen and (min-width: 76.25em) {
  .md-main__inner {
    max-width: none;
  }
  .md-sidebar--primary {
    left: 0;
  }
  .md-sidebar--secondary {
    right: 0;
    margin-left: 0;
    -webkit-transform: none;
    transform: none;   
  }
}
</style>

# Submission Infrastructure Nodes (Automatically updated)

## CERN Machines

Pool|Hostname|Hostgroup|Environment|Condor Version|GWMS Version
----|--------|---------|-----------|--------------|------------
tier0|vocms005.cern.ch|vocmssi/schedd_htcondor/si_test/tier0|qa|23.0.6|
itbdev|vocms0801.cern.ch|vocmssi/glideinwms/gwmsfrontend/itbdev|qa|24.0.4|3.10.10
itb|vocms0802.cern.ch|vocmssi/glideinwms/gwmsfrontend/itb|qa|24.0.3|3.10.10
itb|vocms0803.cern.ch|vocmssi/si_htcondor/ccb/itb|qa|24.0.3|
global|vocms0804.cern.ch|vocmssi/glideinwms/gwmsfrontend/global|cmssi_prod|24.0.3|3.10.10
itb|vocms0808.cern.ch|vocmssi/si_htcondor/central_manager/itb|qa|24.0.3|
itb|vocms0809.cern.ch|vocmssi/schedd_htcondor/si_test/itb|qa|24.0.3|
itb|vocms0811.cern.ch|vocmssi/schedd_htcondor/si_test/itb|qa|24.0.3|
vocmssi|vocms0814.cern.ch|vocmssi|cmssi_prod_cmsbuild||
global|vocms0815.cern.ch|vocmssi/si_htcondor/ccb/global|cmssi_prod|24.0.3|
itb|vocms0816.cern.ch|vocmssi/si_htcondor/ccb/itb|qa|24.0.3|
itb|vocms0822.cern.ch|vocmssi/schedd_htcondor/si_test/itb|qa|24.0.3|
tier0|vocms0824.cern.ch|vocmssi/si_htcondor/central_manager/tier0|cmssi_prod|24.0.3|
tier0|vocms0825.cern.ch|vocmssi/si_htcondor/ccb/tier0|cmssi_prod|24.0.3|
tier0|vocms0826.cern.ch|vocmssi/glideinwms/gwmsfrontend/tier0|cmssi_prod|24.0.3|3.10.10
volunteer|vocms0830.cern.ch|vocmssi/si_htcondor/central_manager/volunteer|cmssi_prod|24.0.3|3.10.10
condormon|vocms0850.cern.ch|vocmssi/si_htcondor/condormon|qa|23.0.6|
monitor_antonio|vocms0851.cern.ch|vocmssi/si_htcondor/monitor_antonio|qa|10.0.1|
global|vocms4100.cern.ch|vocmssi/si_htcondor/central_manager/global|cmssi_prod|24.0.3|


## FNAL Machines

| Pool   | Hostname            | Role                          | Condor Version | GWMS Version |
| ------ | ------------------- | ----------------------------- | -------------- | ------------ |
| global | cmssrvz02.fnal.gov  | Backup Collector & Negotiator | 23.0.12-1      |              |
| global | cmssrvz03.fnal.gov  | Condor connection broker      | 23.0.12-1      |              |
| global | cmssrvz05.fnal.gov  | Backup frontend               | 23.0.12-1      | 3.10.5-1     |
| itb    | cmssrv623.fnal.gov  | Backup Collector & Negotiator | 23.0.12-1      |              |
| itb    | cmssrv628.fnal.gov  | Backup frontend               | 23.0.12-1      | 3.10.5-1     |
| cern   | cmssrv2222.fnal.gov | Backup Collector & Negotiator | 23.0.12-1      |              |
| cern   | cmssrv2221.fnal.gov | Condor connection broker      | 23.0.12-1      |              |
| cern   | cmssrvz16.fnal.gov  | Backup frontend               | 23.0.12-1      | 3.10.5-1     |

### FNAL Schedds

| Pool   | Host                      | Service          | HTCondor version |
| ------ | ------------------------- | ---------------- | ---------------- |
| itb    | cmsgwms-submit1.fnal.gov  | prod schedd, Alma9 | 23.0.12-1      |
| global | cmsgwms-submit9.fnal.gov  | prod schedd, Alma9 | 23.0.12-1      |
| global | cmsgwms-submit10.fnal.gov | prod schedd, Alma9 | 23.0.12-1      |
| global | cmsgwms-submit11.fnal.gov | prod schedd, Alma9 | 23.0.12-1      |
| global | cmsgwms-submit12.fnal.gov | prod schedd, Alma9 | 23.0.12-1      |
| global | cmsgwms-submit13.fnal.gov | prod schedd, Alma9 | 23.0.12-1      |
| global | cmsgwms-submit14.fnal.gov | prod schedd, Alma9 | 23.0.12-1      |
