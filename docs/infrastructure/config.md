# Configuration Management

Puppet is the configuration management system of choice at CERN and its used to
manage all machines.

For documentation on how to manage the servers please refer to the [CERN
configuration management documentation](https://config.docs.cern.ch/)
For further help with puppet please refer to the [Puppet documentation](https://www.puppet.com/docs/puppet/latest)

!!! danger

    If you are new to using Puppet at a large organization you MUST complete the
    [CERN configuration management
    training](https://configtraining.web.cern.ch/configtraining/introduction/standalone.html) before making
    any changes

## Hostgroups

Submission infrastructure manages its machines in its own hostgroup, vocmssi.
The puppet manifests for the hostgroup can be found in the
[it-hostgroup-vocmssi repository](https://gitlab.cern.ch/ai/it-puppet-hostgroup-vocmssi)

The hostgroup contains the following subhostgroups for each type of machine:

* `vocmssi/glideinwms/gwmsfrontend` -> GlideinWMS Frontend machines
* `vocmssi/schedd_htcondor/scale_test` -> ITB Scale test schedds
* `vocmssi/schedd_htcondor/si_test` -> Test schedds
* `vocmssi/si_htcondor/ccb` -> HTCondor conection brokers
* `vocmssi/si_htcondor/central_manager` -> HTCondor Central Manager
* `vocmssi/si_htcondor/condormon` -> Monitoring node, mainly for Grafana
* `vocmssi/si_htcondor/monitor_antonio` -> Node managed by Antonio, do not touch

For each hostgroup in the list mentioned above there is an additional level
that defines the pool the hosts belong in. For example, a host that is in the
`vocmssi/si_htcondor/central_manager/global` hostgroup is the central manager
machine for the global pool.

See the [node list](../nodes.md) for the full set of machines.

## Modules

Submission infrastructure manages two puppet modules, the vocmsgwmsfrontend
module that installs and configures the [GlideinWMS
Frontend](https://gitlab.cern.ch/ai/it-puppet-module-vocmsgwmsfrontend) service
and the [vocmshtcondor
module](https://gitlab.cern.ch/ai/it-puppet-module-vocmshtcondor) that installs
and configure htcondor in a variety of situations, including all machines in
Submission Infrastructure and in schedds (see the next section).

!!! info

    The vocmshtcondor module especially but in part also the gwmsfrontend module lack any real documentation and are pending a full rewrite due to a significant amount of unaddressed technical debt.

## Schedds

Schedds are only partly managed by Submission Infrastructure and are primarily
managed by the teams that use them, so in that sense we do not own the
hostgoups the schedds are placed in but we manage the condor part of the
configuration with the [vocmshtcondor
module](https://gitlab.cern.ch/ai/it-puppet-module-vocmshtcondor) that the
schedd operators are asked to import.

## Secrets

There are 2 categories of secrets that are being managed. a) passwords (aka jwt
signing keys) used by Condor and GlideinWMS to generate and verify tokens and
b) the tokens that are used by the condor daemons to authenticate themselves.

Secrets are stored in the Teigi secret storage service. Refer to the
[configdocs](https://configdocs.web.cern.ch/secrets/index.html) for details.
Secrets are stored and access-controlled on a hostgroup level. The users
allowed to access them is the same set of users that have write access to the
hostgroup (hint: use `ai-pwn show <hg>` to find out).
To place them in the correct place the teigi puppet module is used.

The naming scheme for the secret is as follows:

[passsword|token]\_[pool]\_[secret_name]

e.g.:

* `password_global_frontend`, the signing key for the global pool
* `token_itb_factory0205`: The token for the itb frontend to authenticate to factory0205.

You can list the available secrets with the following command:
```
$ tbag showkeys --hg vocmssi/glideinwms/gwmsfrontend
```

Keep in mind that in some cases, secrets have been placed with the node itself and not in the hostgroup.
```
tbag showkeys --host vocms0313.cern.ch
```
