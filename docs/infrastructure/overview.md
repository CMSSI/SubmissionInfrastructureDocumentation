# Infrastructure Overview

The Submission Infrastructure is divided into pools, each of which contains a
different sent of computing resources to satisfy the requirements of different
jobs. The entry point for compute jobs into the infrastructure are the
schedulers which are divided per-workflow management system.

Most of the infrastructure runs in virtual machines, with the exception of the
central manager of the Global and ITB (Internal-Testbed) pools which are
physical servers.

## Pools

A pool is a set of compute resources and a dedicated system to manage those
resources. In essence, each pool has its own schedulers, central manager and 
GlideinWMS frontend. A scheduler may be connected to more than one pool at a
time, which is the case for most CRAB and production schedds.

Each pool is composed of a set of schedds (note each schedd can be connected to
multiple pools), a Central Manager machine which runs the collector and
negotiator and optionally connection brokers to manage the large number of
active connections each pool requires. You can get a list of nodes including
their hostnames in the Nodes list.

![Pools overview](./../img/pools.png)

The main pools operated at CMS are the following:

### Global Pool

This is the main pool where the vast majority of compute resources lie, it
contains the combined resources of all Grid sites that have allocated computing
capacity for CMS as well as resources from opportunistic supercomputing centres.

### CERN Pool

This pool containes compute resources from the datacenter(s) located at CERN
(i.e. building 513, LHCb compute containers etc) and
CMS-specific compute power in the form of the old trigger farm that was
repurposed for offline jobs. It is internally referred to as the tier0 pool in
code and configuration files.

### ITB (Internal Test Bed)

The ITB pool is a mirror-replica of the global pool aimed at testing changes
before they are put into production. As such, the central manager for this pool
is also a physical machine with identical specifications to the global pool one
which makes it ideal for scale-testing to verify that our systems can handle
future demands.

### Volunteer Pool

The volunteer pool contains volunteer compute resources that are not considered
reliable enough to be put in the global pool, specifically they are individuals
running the [CMS@Home](https://lhcathome.web.cern.ch/projects/cms) client to
volunteer their unused cpu power.

### ITB-Dev

The ITB-Dev pool is a rudementary pool used for testing extremely experimental
changes and for training.
