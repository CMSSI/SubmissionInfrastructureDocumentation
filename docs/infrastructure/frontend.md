# GlideinWMS Frontend

[GlideinWMS](https://glideinwms.fnal.gov/doc.prd/index.html) is a workload
management system that automatically scales the size of the resource pool
according to the current resource requirements.

The frontend is configured via an XML configuration file and since, as described
in the [infrastructure overview](./overview.md) section, multiple GlideinWMS
frontend instances are running multiple variations of the configuration is
needed. Additionally, there is the complication that every frontend instance has
a backup/failover instance running at FermiLab which does not share the puppet
environment with CERN. As a result we cannot manage the GlideinWMS configuration
values through the standard methods. However, any update to a configuration file
should also be reflected by an update in the failover frontend and vice-versa.

The configuration files are managed in the
[cmsgwms-frontend-configurations](https://gitlab.cern.ch/CMSSI/cmsgwms-frontend-configurations)
repository. All frontends clone that repository and use their respective
configuration file from the `compiled` directory. For instance, the global pool
frontend at CERN uses the `compiled/cern/global/frontend.xml` configuration file.

A [Kapitan](https://kapitan.dev/) based configuration management system is in
place that abstracts the XML layer into a hierarchical yaml configuration.

Refer to the [Kapitan docs](https://kapitan.dev/pages/kapitan_overview/) fore
more information about how it works. The Jinja2 input system is used with a
standard GlideinWMS configuration file as a template and variables are modified
in that as needed by the different frontends.

The targets (`inventory/targets`) describe the configuration targets (read:
output configuration files) and the classes each includes while the classes
(`inventory/classes`) contains the variables that are modified in each
configuration file with later classes taking precedent over previous ones.

There are 2 branches in the repository, one meant for use at CERN and one at
FNAL. They are separated for policy reasons, to ensure that a review by the
person responsible at each site is done before a change is put into production.
Each time a configuration is modified an automated CI job compares the updated
compiled directory files and sends a PR to the other branch if a change is
detected in the compiled files of the other site. e.g. A change that only
affects `compiled/cern` pushed to the `cern` branch should not trigger an MR,
but a change that affects `compiled/fnal` (among others) pushed to `cern` will
trigger an MR to the `fnal` branch to request approval.

!!! note "Testing glidein_wrappers"

    To test a glidein-wrapper-script place it in a tmp-dir, along with the
    `glidein_startup.sh`-script containing the `token.tgz` and the [generated
    token](../tokens.md) (which has to be named as `credential_*.idtoken`).
    Then execute the wrapper script: `env -i PATH=$PATH
    _CONDOR_SCRATCH_DIR=$PWD TMPDIR=$PWD ./<pool>_glideinwrapper_<name>`
