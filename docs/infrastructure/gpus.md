# Submission Infrastructure and GPU support

CMS has access to opportunistic GPU resources. This is a list of sites (and their entries) that provides GPU support in CMS (as of February 15th):

    [mmascher@lxplus948 ~]$ _condor_SEC_CLIENT_AUTHENTICATION=OPTIONAL _condor_SEC_CLIENT_INTEGRITY=OPTIONAL _condor_SEC_CLIENT_ENCRYPTION=OPTIONAL condor_status -pool vocms0205.cern.ch -any -const 'MyType=="glidefactory" && stringlistmember("CMSGPU",GLIDEIN_Supported_VOs)' -af GLIDEIN_CMSSite EntryName
    T1_DE_KIT CMSHTPC_T1_DE_KIT_cloud-htcondor-ce-1-kit_gpu
    T1_DE_KIT CMSHTPC_T1_DE_KIT_cloud-htcondor-ce-2-kit_gpu
    T1_ES_PIC CMSHTPC_T1_ES_PIC_ce13-multicore_gpu
    T1_IT_CNAF CMSHTPC_T1_IT_CNAF_CINECA_Marconi100_gpu
    T1_IT_CNAF CMSHTPC_T1_IT_CNAF_VEGA_gpu
    T2_CH_CERN_HLT CMSHTPC_T2_CH_CERN_HLT_gpu
    T2_CH_CERN CMSHTPC_T2_CH_CERN_ce506_gpu
    T2_IT_Bari CMSHTPC_T2_IT_Bari_recas_ce03_gpu
    T2_UK_London_IC CMSHTPC_T2_UK_London_IC_ceprod00_gpu
    T2_UK_London_IC CMSHTPC_T2_UK_London_IC_ceprod01_gpu
    T2_UK_London_IC CMSHTPC_T2_UK_London_IC_ceprod02_gpu
    T2_UK_London_IC CMSHTPC_T2_UK_London_IC_ceprod03_gpu
    T2_US_Caltech CMSHTPC_T2_US_Caltech_cit2_gpu
    T2_US_Caltech CMSHTPC_T2_US_Caltech_cit3_gpu
    T2_US_Caltech CMSHTPC_T2_US_Caltech_cit_gpu
    T2_US_Florida CMSHTPC_T2_US_Florida_condor_gpu
    T2_US_MIT CMSHTPC_T2_US_MIT_ce01_gpu
    T2_US_MIT CMSHTPC_T2_US_MIT_ce02_gpu
    T2_US_MIT CMSHTPC_T2_US_MIT_ce03_gpu
    T2_US_Purdue CMSHTPC_T2_US_Purdue_Hammer_Slurm_gpu
    T2_US_Wisconsin CMSHTPC_T2_US_Wisconsin_cmsgrid01_gpu
    T2_US_Wisconsin CMSHTPC_T2_US_Wisconsin_cmsgrid02_gpu
    T2_US_Wisconsin CMSHTPC_T2_US_Wisconsin_cmsgrid03_gpu
    T3_UK_London_QMUL CMSHTPC_T3_UK_London_QMUL_lcg_arcce02_gpu
    T3_UK_London_QMUL CMSHTPC_T3_UK_London_QMUL_lcg_arcce03_gpu
    T2_US_Vanderbilt CMS_T2_US_Vanderbilt_ce5_gpu
    T2_US_Vanderbilt CMS_T2_US_Vanderbilt_ce6_gpu
    T3_US_OSG Glow_US_Syracuse2_condor_gpu
    T3_US_OSG Glow_US_Syracuse3_condor_gpu
    T3_US_OSG Glow_US_Syracuse4_condor_gpu

Resources configured in the GlideinWMS factory do not provide detailed information about GPUs, they are only available after the pilots starts running and it connects to the Global pool. The [GPUs monitor dashboard](https://monit-grafana.cern.ch/d/2qoPfS0Mz/cms-submission-infrastructure3a-gpus-monitor?orgId=11) provides a detailed view of all the GPUs that connected to the Global Pool during a certain period of time. Probe pilots are periodically sent to the sites in order to provide information even if there are no requests from CMS jobs. This is a [sample document](Sample%20document:%20https://monit-opensearch.cern.ch/dashboards/app/discover#/doc/AWcN2jgOKytUtEPdzJQv/monit_prod_cms_raw_si_condor_startd-2024-02-13?id=be34c89d-6db3-fc56-30e1-b723b3cd3fb3).


# Details about the "GPUs monitor dashboard"

This is a list of information provided by the [condor_gpu_discovery tool](https://htcondor.readthedocs.io/en/latest/man-pages/condor_gpu_discovery.html)

|Attribute name|Sample Value|
|--|--|
|GPUs|2|
|GPUs_Capability|7.5|
|GPUs_ClockMhz|1590|
|GPUs_ComputeUnits|40|
|GPUs_CoresPerCU|64|
|GPUs_DeviceName|Tesla T4|
|GPUs_DriverVersion|12.3|
|GPUs_ECCEnabled|true|
|GPUs_GlobalMemoryMb|14930|
|GPUs_MaxSupportedVersion|12030|


In addition to those, a the support of few more attributes has been requested and provided throug the [si_cuda_supported_runtimes.sh](https://gitlab.cern.ch/CMSSI/CMSglideinWMSValidation/-/blob/production/si_cuda_supported_runtimes.sh?ref_type=heads) pilot validation script.
This in turns make use of a CMS specific tool that discover the CMS CUDA supported runtimes that is available at `/cvmfs/cms.cern.ch/cuda-compatible-runtime/x86_64/rhel7/latest/`

|Attribute name|Sample Value|
|--|--|
|CMS_CUDA_SUPPORTED_RUNTIMES|10.0,10.1,10.2,11.0,11.1,11.2,11.3,11.4,11.5|
|CMS_NVIDIA_DRIVER_VERSION|545.23.08|

Finally, an attribute has been requested to facilitate matchmaking in WMAgent:
https://its.cern.ch/jira/projects/CMSSI/issues/CMSSI-79

Evaluations are ongoing to determine if it is better to create a new attribute (`int(split(GPUs_Capability,".")[0])*1000+int(split(GPUs_Capability,".")[1])*10+len(split(GPUs_Capability,".")[0])`) or use `versioncmp`

# Useful links
Nvidia Documentation: https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html
