# Tokens

HTCondor relies on jwt tokens to authenticate users, compute daemons and any
other services it relies on.

![Secrets](../img/secrets.png)

Each pool has 2 different set of signing keys, the `FRONTEND` key that is used
to sign tokens that are distributed to the pilots in order for them to be able
to authenticate and join the pool, and the pool key (usually named `<pool
name>_pool_key`, e.g. `global_pool_key`) that is used to sign tokens for
schedds and other daemons to access the pool.

## Generating tokens

Tokens can be generated via the `condor_token_create` command. See the relevant
manual page for the required parameters. In order to generate a token log into
any machine that is joined in the pool you want the token to be able to access
and run `condor_token_create -key <pool_name>_pool_key ...`

!!! info

    It is good practice to always have an expiration time to tokens, unless
    they are supposed to be indefinite, like schedd tokens.

!!! warning

    In order to be able to trace who has access to the pool at any given time,
    every time a request for a new token is fulfilled the details of the token
    should be filed in the [token table](../tokens.md) 

### Requesting tokens
Tokens can also be requested from another pool for authentication.

!!! example

    Requesting a token from the factory `gfactory-1.osg-htc.org` is done by

    ```
    _condor_SEC_CLIENT_AUTHENTICATION_METHODS=SSL condor_token_request
        -pool gfactory-1.osg-htc.org
        -token tier0_tier0_gfactory-1.idtoken
        -identity fecmscernt0@gfactory-1.osg-htc.org
    ```

    This starts a process and outputs the ID of the request to be approved on
    the factory. Once this is done, the token is placed in
    `/etc/condor/tokens.d/tier0_tier0_gfactory-1.idtoken`.          

## Distributing tokens

Daemon tokens are distributed via puppet, using [tbag/teigi](https://configdocs.web.cern.ch/secrets/index.html).
See also [here](config.md#secrets).

!!! example

    A token is created on the central manager of the pool, e.g. for a schedd:

    ```
    condor_token_create --key <pool_name>_pool_key -authz ADVERTISE_SCHEDD -authz ADVERTISE_MASTER -authz READ -identity <schedd_name> -lifetime <limited_lifetime>
    ```

    and then added to `tbag` ond `aiadm`:

    ```
    tbag set (--host <schedd_host>|--hg <hostgroup>) --file <token_file> token_<pool_name>_<schedd_host>
    ```
