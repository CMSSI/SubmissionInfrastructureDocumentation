# Matchmaking

Matchmaking in Condor is the process that assigns user jobs from Access Points (APs or schedulers) to Execute Points (EPs or worker nodes). Both jobs in APs and machines in EPs set their own custom matchmaking expressions. Jobs do this using the `Requirements` classad, while machines use the `START` expression.

Jobs' requirements are set in WMAgent/CRAB/CMS Connect APs, either by a configuration knob of the scheduler (`SYSTEM_PERIODIC_REMOVE`, set up by Submission Infrastructure), or by WMAgent/CRAB/CMS Connect themselves.

Machines' `START` expressions are automatically set by Condor. Additionally, part of the expression is defined in the GlideinWMS frontend and propagated through the pilot to the startd running on the EP.

The above is standard `GlideinWMS/HTCondor` functionality. As we will see below, CMS Submission Infrastructure has implemented a custom matchmaking mechanism that site admins can use to advertise custom site abilities, both for monitoring and for steering jobs to their site.

# Custom Matchmaking

The first method a site admin can use to select which jobs run at their site is the ability to specify a custom matchmaking expression. This is done by looping over a specific file in the `SITECONF` that contains the bits to be appended to the machine's `START` expression. The file in question can be customized by the site admin and is usually located at `/cvmfs/cms.cern.ch/SITECONF/local/GlideinConfig/local-start.txt`. [This](https://gitlab.cern.ch/CMSSI/CMSglideinWMSValidation/-/blob/5fb39aeb2414a19676a3eba0b88d9c6eb182d0a1/export_siteconf_info.py#L99-113) is the script that handles the customization. N.B.: A site has to be enabled centrally, otherwise it won't go through the start expression customization phase. Please get in touch with Submission Infrastructure (SI) if you are interested in enabling this feature.

The second method that might be used by a site admin for matchmaking is the ability to send custom machine classads to the CMS Collector. This is done by looking at the `SITECONF`, looping through the files in the `ClassAds` directory (e.g., `/cvmfs/cms.cern.ch/SITECONF/local/ClassAd`). Each file in that directory is expected to contain shell scripts that are sourced. The filenames are also used to identify the name of the classad. For example, a file named `CLASSADFILE` must set a variable named `CLASSADFILE`. `CLASSADFILE=$CLASSADFILE` will be the classad advertised in the collector. [This](https://gitlab.cern.ch/CMSSI/CMSglideinWMSValidation/-/blob/production/advertise_site_classads.sh) is the script that performs the custom classad advertising.

